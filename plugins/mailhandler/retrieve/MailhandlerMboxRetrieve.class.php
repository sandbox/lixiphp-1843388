<?php
/**
 * @file
 * Definition of MailhandlerMboxRetrieve class.
 */

/**
 * Retrieve messages from a Mailhandler Mailbox.
 */
class MailhandlerMboxRetrieve extends MailhandlerRetrieve {
  /**
   * Connect to mailbox and run message retrieval.
   *
   * @param object $mailbox
   *   The mailbox to retrieve from.
   * @param string|null $filter_name
   *   (optional) Mailhandler filter to restrict what messages are retrieved.
   *
   * @return array
   *   Retrieved messages.
   */
  function retrieve($mailbox, $filter_name = 'MailhandlerFilters') {
    extract($mailbox->settings);
    
    $is_local = ($type == 'local');
    if(!empty($folder) && $folder != 'INBOX') {
      if(file_exists(DRUPAL_ROOT.'/'.$folder)) {
        $folder = DRUPAL_ROOT.'/'.$folder;
      }
      $folder_is_set = TRUE;
    }
    else {
      $folder_is_set = FALSE;
    }
    $connect_is_set = !empty($domain) && !empty($port) && !empty($name) && !empty($pass);
    
    $messages = array();

    if ($is_local && $folder_is_set) {
      require_once 'Mail/Mbox.php';
      require_once 'Mail/mimeDecode.php';
      
      $mbox = new Mail_Mbox($folder);
 
      $mbox->open();
      $size = $mbox->size();
      for ($n = 0; $n < $mbox->size(); $n++) {
        $message = $mbox->get($n);
        $decodedMessage = new Mail_mimeDecode($message, "\r\n");
        $structuredMessage = $decodedMessage->decode(
          array(
            'include_bodies'    =>  true,
            'decode_bodies'     =>  true
          )
        );
        
        $body = '';
        $mimeparts = array();
        if(strtolower(trim($structuredMessage->ctype_primary)) == 'multipart') {
          foreach($structuredMessage->parts as $parts) {
            if($parts->ctype_primary == 'text') {
              $body = $parts->body;
              continue;
            }
            if(isset($parts->ctype_parameters['name'])) {
              $mimeparts[] = (object) array(
                'headers' => $parts->headers,
                'attributes' => $parts->headers,
                'filename' => $parts->ctype_parameters['name'],
                'filemime' => $parts->ctype_primary.'/'.$parts->ctype_secondary,
                'extname' => $parts->ctype_secondary,
                'data' => utf8_encode($parts->body),
              );
            }
          }
          //watchdog('mailhandler', 'mbox mimeparts !arr', array('!arr' => print_r($mimeparts, TRUE)), WATCHDOG_INFO);
        }
        else {
          $body = $structuredMessage->body;
          
        }
        
        $user = '[a-zA-Z0-9_\-\.\+\^!#\$%&*+\/\=\?\`\|\{\}~\']+';
        $domain = '(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.?)+';
        $ipv4 = '[0-9]{1,3}(\.[0-9]{1,3}){3}';
        $ipv6 = '[0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7}';
        
        $mail_from = array();
        $from_obj = NULL;
        if(valid_email_address($structuredMessage->headers['from'])) {
          $mail_from = explode('@', $structuredMessage->headers['from']);
          $from_obj = new stdclass();
          $from_obj->mailbox = $mail_from[0];
          $from_obj->host = $mail_from[1];
        }
        else {
          $from_str = str_replace("\\",'',$structuredMessage->headers['from']);
          preg_match("/\"*([a-zA-Z0-9_\-\(\)\s]+)\"*\s*<($user)@($domain|\[$ipv4|$ipv6\])>/i", $from_str, $mail_from);
          $from_obj = new stdclass();
          $from_obj->personal = $mail_from[1];
          $from_obj->mailbox = $mail_from[2];
          $from_obj->host = $mail_from[3];
        }
        
        $structuredMessage->headers['fromaddress'] = $structuredMessage->headers['from'];
        $structuredMessage->headers['from'] = array( 0 => $from_obj );
        $structuredMessage->headers['message_id'] = $structuredMessage->headers['message-id'];
        
        if(isset($structuredMessage->headers['cc'])) {
          $cc_arr = explode(', ', $structuredMessage->headers['cc']);
          $cc_address = array();
          if($cc_arr) {
            $mail_cc = array();
            foreach($cc_arr as $cc_str) {
              if(valid_email_address($cc_str)) {
                $mail_cc = explode('@', $cc_str);
                $cc_address[] = (object) array(
                  'mailbox' => $mail_cc[0],
                  'host' => $mail_cc[1],
                );
              }
              else {
                $cc_str = str_replace("\\",'',$cc_str);
                preg_match("/\"*([a-zA-Z0-9_\-\(\)\s]+)\"*\s*<($user)@($domain|\[$ipv4|$ipv6\])>/i", $cc_str, $mail_cc);
                if($mail_cc) {
                  $cc_address[] = (object) array(
                    'personal' => $mail_cc[1],
                    'mailbox' => $mail_cc[2],
                    'host' => $mail_cc[3],
                  );
                }
              }
            }
          }
          $structuredMessage->headers['ccaddress'] = $structuredMessage->headers['cc'];
          $structuredMessage->headers['cc'] = $cc_address;
        }
        
        if(isset($structuredMessage->headers['to'])) {
          $to_arr = explode(', ', $structuredMessage->headers['to']);
          $to_address = array();
          if($to_arr) {
            $mail_to = array();
            foreach($to_arr as $to_str) {
              if(valid_email_address($to_str)) {
                $mail_to = explode('@', $to_str);
                $to_address[] = (object) array(
                  'mailbox' => $mail_to[0],
                  'host' => $mail_to[1],
                );
              }
              else {
                $to_str = str_replace("\\",'',$to_str);
                preg_match("/\"*([a-zA-Z0-9_\-\(\)\s]*)\"*\s*<*($user)@($domain|\[$ipv4|$ipv6\])>*/i", $to_str, $mail_to);
                $to_address[] = (object) array(
                  'personal' => $mail_to[1],
                  'mailbox' => $mail_to[2],
                  'host' => $mail_to[3],
                );
              }
            }
          }
          $structuredMessage->headers['toaddress'] = $structuredMessage->headers['to'];
          $structuredMessage->headers['to'] = $to_address;
        }
        
        if(isset($structuredMessage->headers['references'])) {
          preg_match_all("/(<.*>)/Ui", $structuredMessage->headers['references'], $mail);
          $structuredMessage->headers['references'] = $mail[0];
        }
        
        if(isset($structuredMessage->headers['in-reply-to'])) {
            //exist references array
            $arr_references = array();
            if(isset($structuredMessage->headers['references']) && is_array($structuredMessage->headers['references']) && !in_array($structuredMessage->headers['in-reply-to'], $structuredMessage->headers['references'])) {
                $structuredMessage->headers['references'][] = $structuredMessage->headers['in-reply-to'];
            }
            else {
                preg_match_all("/(<.*>)/Ui", $structuredMessage->headers['in-reply-to'], $mail);
                $structuredMessage->headers['references'] = $mail[0];
            }
        }
        
        if(!isset($structuredMessage->headers['subject']) && strpos($structuredMessage->headers['date'], 'Subject')) {
          preg_match("/(.*)\s+(Subject:)+(.*)$/i", $structuredMessage->headers['date'], $date_s);
          $structuredMessage->headers['date'] = $date_s[1];
          $structuredMessage->headers['subject'] = $date_s[3];
        }
        
        if(isset($structuredMessage->headers['subject'])) {
          $structuredMessage->headers['subject'] = mb_decode_mimeheader($structuredMessage->headers['subject']);
        }
        
        if(isset($structuredMessage->headers['x-uid'])) {
          $structuredMessage->headers['Msgno'] = $structuredMessage->headers['x-uid'];
        }
        
        //$body = preg_replace('/(>+\f*)[\r\n]/', '$1', $body, 1);
        $body = preg_replace("/<([^>]*)([\r\n])[\s]+/i", '<$1 ', $body);
        
        //watchdog('mailhandler', 'mbox Header array is !arr', array('!arr' => print_r($structuredMessage->headers, TRUE)), WATCHDOG_INFO);
        
        $messages[] = array(
          'header' => (object) $structuredMessage->headers,
          'body_text' => utf8_encode($body),
          'body_html' => utf8_encode($body),
          'mimeparts' => $mimeparts,
        );
      }
      
      $mbox->close();
    }
    else {
      throw new Exception(t('Unable to connect mbox. Please check the connection settings for this mbox.'));
    }

    //watchdog('mailhandler', 'mbox was checked and contained %retrieved messages.', array('%retrieved' => $size), WATCHDOG_INFO);
    
    return $messages;
  }

  /**
   * Test connection to a mailbox.
   *
   * @param object $mailbox
   *   The mailbox to test.
   *
   * @return array
   *   Test results.
   */
  function test($mailbox) {
    extract($mailbox->settings);
    $ret = array();

    $is_local = ($type == 'local');
    $connect_is_set = !empty($domain) && !empty($port) && !empty($name) && !empty($pass);
    
    if(!empty($folder) && $folder != 'INBOX') {
      if(file_exists(DRUPAL_ROOT.'/'.$folder)) {
        $folder = DRUPAL_ROOT.'/'.$folder;
      }
      $folder_is_set = TRUE;
    }
    else {
      $folder_is_set = FALSE;
    }

    if ($is_local && $folder_is_set) {
      require_once 'Mail/Mbox.php';
      require_once 'Mail/mimeDecode.php';
      
      $mbox = new Mail_Mbox($folder);
 
      $mbox->open();
      
      $size = $mbox->size();
      
      if($size) {
        $ret[] = array('severity' => 'status', 'message' => t('There are @messages messages in the mailbox folder.', array('@messages' => $size)));
      }
      else {
        $ret[] = array('severity' => 'warning', 'message' => t('Mailhandler could not open the specified folder'));
      }
      $mbox->close();
    }
    elseif($connect_is_set) {
      if ($result = $this->open_mailbox($mailbox)) {
        $ret[] = array('severity' => 'status', 'message' => t('Mailhandler was able to connect to the mailbox.'));
        $box = $this->mailbox_string($mailbox);
        $status = imap_status($result, $box, SA_MESSAGES);
        if ($status) {
          $ret[] = array('severity' => 'status', 'message' => t('There are @messages messages in the mailbox folder.', array('@messages' => $status->messages)));
        }
        else {
          $ret[] = array('severity' => 'warning', 'message' => t('Mailhandler could not open the specified folder'));
        }
        $this->close_mailbox($result);
      }
      else {
        imap_errors();
        $ret[] = array('severity' => 'error', 'message' => t('Mailhandler could not access the mailbox using these settings'));
      }
    }
    
    return $ret;
  }

  function purge_message($mailbox, $message) {
    
  }
}
