<?php
/**
 * @file
 * MailhandlerMbox class.
 */

$plugin = array(
  'name' => 'Mbox Library',
  'description' => 'Uses the Mbox library to retrieve messages from local mailboxes',
  'handler' => array(
    'class' => 'MailhandlerMboxRetrieve',
    'parent' => 'MailhandlerRetrieve'
  ),
  'file' => 'MailhandlerMboxRetrieve.class.php',
);
